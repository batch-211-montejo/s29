/*
	MongoDB - Query Operators
	Expand Queries in MongoDB using Query Operators

*/

/*
Overview:

Query Operator:
-Definition
Importance

Types of Query Operators:
1. Comparison Query Operators 
	1.1 Greater Than
	1.2 Greater than or equal to
	1.3 Less than
	1.4 LEss than or equal to
	1.5 Not equal to
	1.6 In
2. Evaluation Query Operator
	2.1 Regex
		2.1.1 Case Sensitive Query
		2.1.2 Csae Insensitive Query
3. Logical Query Operators
	3.1 OR
	3.2 AND


Firel Project
1. Inclusion 
	1.1 returning specific fields in embedded docs
	1.2 Exception to the inclusion rure
	1.3 Slice Operator
2. Exclusion
	2.1 Exlcuing specific fields in emedded docs
*/

/*
What does Query Operator Mean?
	-A "Query" is a request for data from a database
	-An "Operator" is a symbol that represents an action or a process
	-Putting them together, they mean the things that we can do on our querirs using certain operators
*/

/*
Why do we need to study Query Operators?
	-Knowing Query Operators will enable us to create querires that can do more than what simple operators fo. (We can ddo more than what we do in simple CRUD Operators)
	-For example, in our CRUD Operation discussion, we have discussed findOne uign a specific value inside its singl eor multiple parameters. When we know query operators, we may look for records that are more specific.
*/

// 1. COMPARISON QUERY OPERATORS
/*
-Includes:
	-greater than
	-less than
	-greater than or equal to
	-less than or equal to
	-not equal to
	-in
*/

/*
1.1 GREATER THAN: "$gt"
	-"$gt" inds docs that have fields numbers that are greater than a specified value
	-Syntax:
		db.collectionName.find{field: ({ $gt: value}})
*/
	db.users.find({age: {$gt: 76}});

/*
1.2 GREATER THAN OT EQUAL TO: "$gte"
	-"$gte" finds documents thta have field number values that are greater than or equal to a specfic value
	-Syntax:
		db.users.find({age: {$gte: 76}})
db.collectionName.find({field: {$gte: value}})
*/
	db.users.find({age: {$gte: 76}});


/*
1.3 LESS THAN: "$lt"
	-"$lt" find dpcs that have field number values less than the specofies value
	-Syntax:
		db.collectionName.find{field: ({ $lt: value}})

*/
	db.users.find({age: {$lt: 65}});

/*
1.4 LESS THAN OR EQUAL TO: "$lte"
	-"$lte" finds docs that have field number values that are less than or equal to a specified value,
	-Syntax:
		db.collectionName.find({field: {$lte: value}})
*/
	db.users.find({age: {$lte: 65}});

/*
1.5 NOT EQUAL TO: "$ne"
	-"$ne" finds docs that have field numbers that are not equal to a specified value
	-Syntax: 
		db.collectionName.find({field: {$ne: value}})
*/
	db.users.find({age: {$ne: 65}});

/*
1.5 In Operator - "$in"
	-"$in" finds docs iwth specific match criteria on one fiels using differen values
	-Syntax:
		db.collectionName.find({field: {$in: value}})
*/
	db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
	db.users.find({courses: {$in: ["HTML", "React"]}});


// 2. EVALUTAION QUERY OPERATORS
// -Evaluation Operators return data based on evaluations of either individual fields or the entire collection's documents


/*
2.1 REGEX OPERATOR: "$regex"
	-Regex is short for regular expression
	-They are called regular expressions because they are based on regular languages
	-Regex is used for matching strings
	-It allows ais to find docs that match a specific string patterm using regular expression
*/

/*
2.1.1 Case sensitive Query
	-Syntax:
		db.collectioname.find({field: {$regex: 'pattern'}})
*/
	db.users.find({lastName: {$regex: 'A'}})

/*
2.1.2 Case insensitive query
	-We can run case insensitive queries by utilizing the "i" option
	-Syntax: 
		db.collectioname.find({field: {$regex: 'pattern', $options: '$optionValue'}})
*/
	db.users.find({lastName: {$regex: 'A', $options: '$i'}})


// Miniactivity
// letter 'e' in the first name using acse insenstive query
db.users.find({firstName: {$regex: 'e', $options: '$i'}})


// 3. LOGICAL QUERY OPERATORS
/*
3.1 OR OPERATOR : "$or"
	-$"or" finds docs that matach a single criterai from multiple proided search criteria
	-Syntax:
		db.collectioname.find({ $or: [ {fieldA: valueA}, {fieldB: valueB}]}})
*/

	db.users.find ({$or: [{firstName: "Neil"}, {age: "25"}]});
	db.users.find ({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]})

/*
3.2 AND OPERATOR: $and
	-"$and" finds docs matching multipel crieteria ina  asingle field
	-Syntax:
		db.collectioname.find({ $and: [ {fieldA: valueA}, {fieldB: valueB}]}})
*/
	db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});


// miniactivity
// find user
db.users.find({$and: [{firstName: {$regex: 'e'}}, {age: {$lte: 30}}]});

// FIELD PROJECTIOM
/*
	-by default, MongoDB returns the whole docs, especially. when dealing with complex decos
	-But sometimes, it is not helpful to view the while doscs, especially. when dealing with complex docs
	-To help with readbility of the values returned (or sometimes, ebcasue of security reasons), we include or exclude some fields
	-in other words, we project our selected fields
*/

/*
1. INCLUSION
	-allows us to include/add specific fields only when retrieving docs
	-The value denoted is "1" tp indicate that the field is being included
	-we cannot fo exclusion on field uses inclusion projecttion-Syntax:
	-Syntax:
		db.collectionName.find({criteria}), {field:1})
*/
 	db.users.find({firstName: "Jane"})
 	db.users.find (
 	{
 		firstName: "Jane"
 	},
 	{
 		firstName: 1,
 		lastName: 1,
 		"contact.phone": 1
 	}
 	)

/*
1.1 RETURNING SPECIFIC FIELDS IN EMBEDDED DOCUMENTS
	-the double quotation are important.
*/

	db.users.find (
	 	{
	 		firstName: "Jane"
	 	},
	 	{
	 		firstName: 1,
	 		lastName: 1,
	 		"contact.phone": 1
	 	}
	 	)

/*
1.2 EXCEPTION TO THE INCLUSION REULE: Supressing the ID Fields
	-allows is to exclude the "_id" field when retrieving docs
	-when using field projection, field inclusion and exclusion may not be used at the same time.
	-Excluding the "id" field is the ONLY exception to this rule
	-Syntax:
		db.collectionName.find({criteria}, {id:0})
*/
	db.users.find (
	 	{
	 		firstName: "Jane"
	 	},
	 	{
	 		firstName: 1,
	 		lastName: 1,
	 		"contact.phone": 1,
	 		_id: 0
	 	}
	 	)

/*
1.3 SLICE OPERATOR: "$slice"
	-"$slice" operator allows us to retrieve only 1 element that matches the seacrh criteria
*/

// tp demonstrate, let us first insert and vie any array
	db.user.insert({
		namearr: [
			{
				namea: "Juan"
			},
			{
				nameb: "Tamad"
			}

		]
	});

	db.user.find({
		namearr: 
		{
			namea: "Juan"
		}
	})

// now, let us use the slice operator
	db.users.find(
			{ "namearr": 
				{ 
					namea: "Juan" 
				} 
			}, 
			{ namearr: 
				{ $slice: 1 } 
			}
		)



// miniactivity

	db.users.find(
	        {
	            firstName: {$regex: 's', $options: 'i'}
	        },
		{
		 		firstName: 1,
		 		lastName: 1,
		 		"contact.phone": 1,
		 		_id: 0
		 }
	)

/*
2. EXCLUSION
	-allows us to exllcude or remove specific fields when retrieving docs
	-the value provided  is zero denote that the field it being included
	-Syntax:
		db.collectionName.find({criteria}, field:0)
*/

	db.users.find(
			{
				firstname: "Jane"
			},
			{
				contact: 0,
				department: 0
			}

		)


/*
2.1 EXCLUDING/SUPRESSING SPECIFIC FIELDS IN EMBEDDED DOCUMENT
*/
	db.users.find (
			{
				firstName: "Jane"
			},
			{
				"contact.phone": 0
			}
		)



